require 'test_helper'

class StaticPageControllerTest < ActionDispatch::IntegrationTest
  test "should get home" do
    get static_page_home_url
    assert_response :success
  end

  test "should get student" do
    get static_page_student_url
    assert_response :success
  end

  test "should get course" do
    get static_page_course_url
    assert_response :success
  end

  test "should get attendance" do
    get static_page_attendance_url
    assert_response :success
  end

end
