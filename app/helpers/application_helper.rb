module ApplicationHelper

  # Returns the full title on a per-page basis.       # Documentation comment
  def full_title(page_title = '')                     # Method def, optional arg
    base_title = "School Management System"  # Variable assignment
    if page_title.empty?                              # Boolean test
      base_title                                      # Implicit return
    else
      page_title + " | " + base_title                 # String concatenation
    end
  end

  def date_dbY(date)
	  if date.nil?
	    ""
	  else
	    date.strftime("%d-%b-%Y")
	  end
	end

  def time_format(time)
    if time.nil?
      ""
    else
      time.strftime('%I:%M %p')
    end
  end

end