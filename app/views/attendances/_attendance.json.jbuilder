json.extract! attendance, :id, :att_id, :user_id, :start_time, :end_time, :date, :remark, :created_at, :updated_at
json.url attendance_url(attendance, format: :json)
