json.extract! course, :id, :course_id, :name, :start_date, :end_date, :course_fee, :created_at, :updated_at
json.url course_url(course, format: :json)
