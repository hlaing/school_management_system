json.extract! user, :id, :user_id, :role_id, :first_name, :last_name, :gender, :phone_no, :email, :password, :date_of_birth, :address, :created_at, :updated_at
json.url user_url(user, format: :json)
