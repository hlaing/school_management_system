class Course < ApplicationRecord
	has_many :student_courses

	validates :name,  presence: true, length: { maximum: 50 }
	validates :start_date,  presence: true
	validates :end_date,  presence: true
  validates :course_fee,  presence: true
end
