class Ability
  include CanCan::Ability

  def initialize(user)
    
      user ||= User.new 
      if user.role_id == 1   #admin
        can :manage, :all    
      else
        can [:read, :edit], User
        can :read, Course
        can :read, Attendance
      end
    
  end
end
