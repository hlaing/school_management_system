class CoursesController < ApplicationController
  before_action :set_course, only: [:show, :edit, :update, :destroy]
  before_action :logged_in_user, only: [:index, :show, :edit, :update, :destroy]

  # GET /courses
  # GET /courses.json
  def index
    @courses = Course.all.paginate(page: params[:page], per_page: 10)
  end

  # GET /courses/1
  # GET /courses/1.json
  def show

    @students = StudentCourse.joins(:user)
      .select("student_courses.*,users.roll_no,first_name,last_name")
      .where(:course_id => params[:id])

    if params[:add_student] == 'Add Student'
      @student_course = StudentCourse.new
      @user = User.find_by_roll_no(params[:student_roll_no])
      @student_course[:user_id] = @user.id
      @student_course[:course_id] = params[:id]
      @student_course[:create_by] = current_user.id
      if @student_course.save
        flash[:success] = "Add student successfully"
      end
    end

    if params[:add_student] == 'Update Student'
      @student_course = StudentCourse.find(params[:student_course_id_update])
      @user = User.find_by_roll_no(params[:student_roll_no])
      if @student_course.update(:user_id => @user.id, 
                                :update_by => current_user.id )
        flash[:success] = "Update student successfully"
      end
    end
    
          
  end

  def student_name
    @user = User.find_by_roll_no(params[:roll_no])
    render :json => @user.to_json    
  end

  # GET /courses/new
  def new
    @course = Course.new
  end

  # GET /courses/1/edit
  def edit
  end

  # POST /courses
  # POST /courses.json
  def create
    @course = Course.new(course_params)
    respond_to do |format|
      if @course.save
        format.html { redirect_to @course}
        format.json { render :show, status: :ok, location: @course }
      else
        format.html { render :edit }
        format.json { render json: @course.errors, status: :unprocessable_entity }
      end
    end

  end

  # PATCH/PUT /courses/1
  # PATCH/PUT /courses/1.json
  def update
    respond_to do |format|
      if @course.update(course_params)
        format.html { redirect_to @course}
        format.json { render :show, status: :ok, location: @course }
      else
        format.html { render :edit }
        format.json { render json: @course.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /courses/1
  # DELETE /courses/1.json
  def destroy
    @course.destroy
    redirect_to courses_url
    flash[:success] = "Course was successfully destroyed."
  end

  def student_destroy
    @student_course = StudentCourse.find(params[:id])
    if @student_course.destroy
      redirect_to courses_url
      flash[:success] = "Student was successfully destroyed for this course."
    end
    
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_course
      @course = Course.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def course_params
      params.require(:course).permit(:course_id, :name, :start_date, :end_date, :course_fee)
    end

    def student_course_params
      params.require(:student_course).permit( :course_id, :user_id, :create_by, 
        :update_by)
    end
end
