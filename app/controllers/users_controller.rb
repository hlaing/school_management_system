class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_action :logged_in_user, only: [:index, :show, :edit, :update, :destroy]

  # GET /users
  # GET /users.json
  def index
    @users = User.where( 'role_id' => '2').paginate(page: params[:page],
       per_page: 3)
  end

  # GET /users/1
  # GET /users/1.json
  def show
    @role = Role.find(@user.role_id)
    @parent_id = Relation.find_by_student_id(params[:id])
    if !@parent_id.nil?
      @parent = User.find(@parent_id.parent_id)
    end  
  end

  # GET /users/new
  def new
    @user = User.new
    @role = Role.all
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)
    @user[:create_by] = @user.id
    
    
      
    respond_to do |format|
      if @user.save
        if params[:student] && @user.role_id == 3
          @student_parent = Relation.new
          @student_parent[:student_id] = params[:student]
          @student_parent[:parent_id] = @user.id
          @student_parent[:create_by] = current_user.id
          @student_parent.save
        end
        format.html { redirect_to @user}
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end 
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    @user[:update_by] = @user.id

    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user}
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end

  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    redirect_to users_url
    flash[:success] = 'User was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit( :role_id, :first_name, :last_name, :roll_no,
       :gender, :nrc, :phone_no, :email, :password, :date_of_birth, :address)
    end

    def role_params
      params.require(:role).permit( :role_id, :role_type )
    end

end

