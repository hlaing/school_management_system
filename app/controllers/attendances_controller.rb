class AttendancesController < ApplicationController
  before_action :set_attendance, only: [:show, :edit, :update, :destroy]
  before_action :logged_in_user, only: [:index, :show, :edit, :update, :destroy]

  # GET /attendances
  # GET /attendances.json
  def index
    if current_user.role_id == 1
      @attendances = Attendance.joins(:user)
        .select("attendances.*,users.roll_no,first_name,last_name")
        .paginate(page: params[:page], per_page: 10)
    elsif current_user.role_id == 2
      @attendances = Attendance.joins(:user)
        .select("attendances.*,users.roll_no,first_name,last_name")
        .where('user_id' => current_user.id )
        .paginate(page: params[:page], per_page: 10)
    else
      @student_id = Relation.find_by_parent_id(current_user.id)
      @attendances = Attendance.joins(:user)
        .select("attendances.*,users.roll_no,first_name,last_name")
        .where('user_id' => @student_id.student_id )
        .paginate(page: params[:page], per_page: 10)
    end
  end

  # GET /attendances/1
  # GET /attendances/1.json
  def show
    @attendance = Attendance.joins(:user)
      .select("attendances.*,users.roll_no,first_name,last_name")
      .find(params[:id])
  end

  # GET /attendances/new
  def new
    @attendance = Attendance.new
  end

  # GET /attendances/1/edit
  def edit
    @attendance = Attendance.joins(:user)
      .select("attendances.*,users.roll_no,first_name,last_name")
      .find(params[:id])
  end

  # POST /attendancesu
  # POST /attendances.json
  def create
    @attendance = Attendance.new(attendance_params)
    @user_id = User.find_by_roll_no( params[:attendance][:roll_no] )
    @attendance[:create_by] = current_user.id
    @attendance[:user_id] = @user_id.id

    respond_to do |format|
      if @attendance.save
        format.html { redirect_to @attendance}
        format.json { render :show, status: :ok, location: @attendance }
      else
        format.html { render :edit }
        format.json { render json: @attendance.errors, status: :unprocessable_entity }
      end
    end

  end

  # PATCH/PUT /attendances/1
  # PATCH/PUT /attendances/1.json
  def update
    @attendance[:start_time] = params[:start_time]
    @attendance[:end_time] = params[:end_time]
    @attendance[:date] = params[:date]
    @attendance[:remark] = params[:remark]
    @attendance[:update_by] = current_user.id
    
    respond_to do |format|
      if @attendance.update(attendance_params)
        format.html { redirect_to @attendance}
        format.json { render :show, status: :ok, location: @attendance }
      else
        format.html { render :edit }
        format.json { render json: @attendance.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /attendances/1
  # DELETE /attendances/1.json
  def destroy
    @attendance.destroy
    redirect_to attendances_url
    flash["success"] = "Attendance was successfully destroyed."
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_attendance
      @attendance = Attendance.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def attendance_params
      params.require(:attendance).permit( :start_time,
        :end_time, :date, :remark, :user_id)
    end
end
