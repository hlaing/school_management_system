class StaticPageController < ApplicationController
  def home
    if current_user 
    	if current_user && current_user.role_id == 1
        @attendances = Attendance.joins(:user)
        	.select("attendances.*,users.roll_no,first_name,last_name")
        	.where('date' => DateTime.now.to_date)
        	.paginate(page: params[:page], per_page: 5)

        @students = User.where( 'role_id' => '2')
        @parents = User.where( 'role_id' => '3')
      elsif current_user.role_id == 2
        @attendances = Attendance.joins(:user)
        	.select("attendances.*,users.roll_no,first_name,last_name")
  	      .where('user_id' => current_user.id )
  	      .paginate(page: params[:page], per_page: 5)
      else
        @student_id = Relation.find_by_parent_id(current_user.id)
        @attendances = Attendance.joins(:user)
  	      .select("attendances.*,users.roll_no,first_name,last_name")
  	      .where('user_id' => @student_id.student_id )
  	      .paginate(page: params[:page], per_page: 5)
   		end
    end
  end

  def student
  end

  def course
  end

  def attendance
  end
end
