# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Role.create!(role_id: 1,
						 role_type: 'Admin'
	)

Role.create!(role_id: 2,
						 role_type: 'Student'
	)

Role.create!(role_id: 3,
						 role_type: 'Parent'
	)

User.create!(first_name:  "Admin",
						 phone_no: "09777888999",
             email: "admin@gmail.com",
             address: "yangon",
             password: "09-111222333",
             gender: "male",
             nrc: "123456",
             role_id: 1,
             roll_no: "1T-1"
             )	