class CreateCourses < ActiveRecord::Migration[5.2]
  def change
    create_table :courses do |t|
      t.string :name
      t.datetime :start_date
      t.datetime :end_date
      t.integer :course_fee
      t.integer :create_by
      t.integer :update_by

      t.timestamps
    end
  end
end
