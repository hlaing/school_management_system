class CreateAttendances < ActiveRecord::Migration[5.2]
  def change
    create_table :attendances do |t|
      t.time :start_time
      t.time :end_time
      t.date :date
      t.text :remark
      t.integer :create_by
      t.integer :update_by
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
