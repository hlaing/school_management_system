class CreateStudentCourses < ActiveRecord::Migration[5.2]
  def change
    create_table :student_courses do |t|
    	t.references :user
    	t.references :course
    	t.integer	:create_by
    	t.integer	:update_by

      t.timestamps
    end
  end
end
