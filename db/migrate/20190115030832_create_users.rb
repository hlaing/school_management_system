class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.integer   :user_id
      t.integer   :role_id
      t.string    :first_name
      t.string    :last_name
      t.string    :roll_no
      t.string    :gender
      t.string    :nrc
      t.string    :phone_no
      t.string    :email
      t.string    :password
      t.datetime  :date_of_birth
      t.text      :address
      t.string    :create_by
      t.string    :update_by

      t.timestamps
    end
  end
end
