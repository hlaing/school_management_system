class CreateRelations < ActiveRecord::Migration[5.2]
  def change
    create_table :relations do |t|
      t.integer :student_id
      t.integer :parent_id
      t.integer :create_by
      t.integer :update_by

      t.timestamps
    end
  end
end
