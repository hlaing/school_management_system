# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_01_22_031121) do

  create_table "attendances", force: :cascade do |t|
    t.time "start_time"
    t.time "end_time"
    t.date "date"
    t.text "remark"
    t.integer "create_by"
    t.integer "update_by"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_attendances_on_user_id"
  end

  create_table "courses", force: :cascade do |t|
    t.string "name"
    t.datetime "start_date"
    t.datetime "end_date"
    t.integer "course_fee"
    t.integer "create_by"
    t.integer "update_by"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "relations", force: :cascade do |t|
    t.integer "student_id"
    t.integer "parent_id"
    t.integer "create_by"
    t.integer "update_by"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "roles", force: :cascade do |t|
    t.integer "role_id"
    t.string "role_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "student_courses", force: :cascade do |t|
    t.integer "user_id"
    t.integer "course_id"
    t.integer "create_by"
    t.integer "update_by"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["course_id"], name: "index_student_courses_on_course_id"
    t.index ["user_id"], name: "index_student_courses_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.integer "role_id"
    t.string "first_name"
    t.string "last_name"
    t.string "roll_no"
    t.string "gender"
    t.string "nrc"
    t.string "phone_no"
    t.string "email"
    t.datetime "date_of_birth"
    t.text "address"
    t.string "create_by"
    t.string "update_by"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "password_digest"
    t.string "remember_digest"
    t.string "reset_digest"
    t.datetime "reset_sent_at"
    t.index ["email"], name: "index_users_on_email", unique: true
  end

end
