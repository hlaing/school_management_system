Rails.application.routes.draw do
  resources :courses
  resources :attendances
  resources :users
  resources :roles
  resources :student_courses
  resources :password_resets,  only: [:new, :create, :edit, :update]
	root 'static_page#home'
  default_url_options :host => "example.com"

  resources :courses do
    member do
      patch :student_destroy
      put :student_destroy
      delete :student_destroy
    end
  end

	get 	 '/home'	,					to: 'static_page#home'
	get 	 '/students',				to: 'users#index'
	get 	 '/courses',				to: 'courses#index'
	get 	 '/attendances',		to: 'attendances#index'
	get    '/signup',  				to: 'users#new'
  post	 '/signup',  				to: 'users#create'
  get    '/login',   				to: 'session#new'
  post   '/login',   				to: 'session#create'
  delete '/logout',  				to: 'session#destroy'
  post   '/studentname',    to: 'courses#student_name'
end
